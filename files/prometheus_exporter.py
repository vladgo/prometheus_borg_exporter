#!/usr/bin/env python

import json
import subprocess
import sys

if len(sys.argv) != 4:
    print "Call script with arguments:"
    print "{} [borg repository locaton] [output textfile collector] [repository name]".format(sys.argv[0])
    sys.exit(1)

repository = sys.argv[1]
textfile_collector = sys.argv[2]
repo_name = sys.argv[3]

archives = json.loads(subprocess.check_output("borg list --json --lock-wait 5 {}".format(repository), shell=True))

info = json.loads(subprocess.check_output("borg info --last 1 --json --lock-wait 5 {}".format(repository), shell=True))

with open(textfile_collector, "w") as f:
    f.write('backup_count{{host="{}",location="{}",repo="{}"}} {}\n'.format(info['archives'][0]['hostname'],
                                                                          repository, repo_name,
                                                                          len(archives['archives'])))
    f.write('backup_files{{host="{}",location="{}",repo="{}"}} {}\n'.format(info['archives'][0]['hostname'],
                                                                          repository, repo_name,
                                                                          info['archives'][0]['stats']['nfiles']))

    backup_last = {'backup_last_size': 'original_size',
                   'backup_last_size_compressed': 'compressed_size',
                   'backup_last_size_dedup': 'deduplicated_size'}
    for bl in backup_last.keys():
        f.write('{}{{host="{}",location="{}",repo="{}"}} {}\n'.format(bl,
                                                                    info['archives'][0]['hostname'],
                                                                    repository, repo_name,
                                                                    info['archives'][0]['stats'][backup_last[bl]]))

    cache_stat = {'backup_chunks_unique': 'total_unique_chunks',
                  'backup_chunks_total': 'total_chunks', 'backup_total_size': 'total_size',
                  'backup_total_size_compressed': 'total_csize',
                  'backup_total_size_dedup': 'unique_csize'}
    for cs in cache_stat.keys():
        f.write('{}{{host="{}",location="{}",repo="{}"}} {}\n'.format(cs, info['archives'][0]['hostname'],
                                                                      repository, repo_name,
                                                                      info['cache']['stats'][cache_stat[cs]]))
