#!/usr/bin/python3

import argparse
import json
import subprocess
import os
import sys
from prometheus_client import start_http_server, Summary
from prometheus_client.core import GaugeMetricFamily, REGISTRY

COLLECTION_TIME = Summary('borg_collector_collect_seconds',
                          'Time spent to collect metrics from Borgbackup')

def parse_args():
    parser = argparse.ArgumentParser(
        description='borg exporter args repository'
    )
    parser.add_argument(
        '-r', '--repository',
        metavar='repository',
        required=True,
        help='borg repository location',
        default=os.environ.get('BORG_REPO')
    )
    parser.add_argument(
        '-n', '--repo-name',
        metavar='repo_name',
        required=True,
        help='borg repository name',
        default=os.environ.get('BORG_REPO_NAME')
    )
    parser.add_argument(
        '-l', '--lock-wait',
        metavar='lock_wait',
        required=False,
        help='wait at most SECONDS for acquiring a repository/cache. Default 1.',
        default=1
    )
    parser.add_argument(
        '-p', '--port',
        metavar='port',
        required=False,
        type=int,
        help='Listen to this port. Default 9118.',
        default=int(os.environ.get('VIRTUAL_PORT', '9118'))
    )
    return parser.parse_args()

class BorgCollector(object):

    def __init__(self, repository, repo_name, lock_wait):
        self.repository = repository
        self.repo_name = repo_name
        self.lock_wait = lock_wait

    @COLLECTION_TIME.time()
    def collect(self):
        list_command = subprocess.getstatusoutput(
            "borg list --json --lock-wait {} {}".format(self.lock_wait, self.repository))
        if list_command[0]:
            return None
        archives = json.loads(list_command[1])
        info_command = subprocess.getstatusoutput(
            "borg info --last 1 --json --lock-wait {} {}".format(self.lock_wait, self.repository))
        if info_command[0]:
            return None
        info = json.loads(info_command[1])

        metrics = {}
        metrics['backup_count'] = GaugeMetricFamily('backup_count', '', labels=['host', 'location', 'repo'])
        metrics['backup_count'].add_metric([info['archives'][0]['hostname'], self.repository, self.repo_name],
                                           len(archives['archives']))

        metrics['backup_files'] = GaugeMetricFamily('backup_files', '', labels=['host', 'location', 'repo'])
        metrics['backup_files'].add_metric([info['archives'][0]['hostname'], self.repository, self.repo_name],
                                           info['archives'][0]['stats']['nfiles'])

        backup_last = {'backup_last_size': 'original_size',
                       'backup_last_size_compressed': 'compressed_size',
                       'backup_last_size_dedup': 'deduplicated_size'}
        for bl in backup_last:
            metrics[bl] = GaugeMetricFamily(bl, '', labels=['host', 'location', 'repo'])
            metrics[bl].add_metric([info['archives'][0]['hostname'], self.repository, self.repo_name],
                                   info['archives'][0]['stats'][backup_last[bl]])

        cache_stat = {'backup_chunks_unique': 'total_unique_chunks',
                      'backup_chunks_total': 'total_chunks', 'backup_total_size': 'total_size',
                      'backup_total_size_compressed': 'total_csize',
                      'backup_total_size_dedup': 'unique_csize'}
        for cs in cache_stat:
            metrics[cs] = GaugeMetricFamily(cs, '', labels=['host', 'location', 'repo'])
            metrics[cs].add_metric([info['archives'][0]['hostname'], args.repository, args.repo_name],
                                   info['cache']['stats'][cache_stat[cs]])

        for m in metrics:
            yield metrics[m]

if __name__ == "__main__":
    try:
        args = parse_args()
        start_http_server(int(args.port))
        print("Polling {}. Serving at port: {}".format(args.repository, args.port))
        REGISTRY.register(BorgCollector(args.repository, args.repo_name, args.lock_wait))
        while True:
            pass
    except KeyboardInterrupt:
        print(" Interrupted")
        sys.exit(0)
