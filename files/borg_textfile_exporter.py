#!/usr/bin/env python3

import argparse
import json
import subprocess
import os
import sys
from prometheus_client import CollectorRegistry, Gauge, write_to_textfile

def parse_args():
    parser = argparse.ArgumentParser(
        description='borg exporter args repository'
    )
    parser.add_argument(
        '-r', '--repository',
        metavar='repository',
        required=True,
        help='borg repository location',
        default=os.environ.get('BORG_REPO')
    )
    parser.add_argument(
        '-n', '--repo-name',
        metavar='repo_name',
        required=True,
        help='borg repository name',
        default=os.environ.get('BORG_REPO_NAME')
    )
    parser.add_argument(
        '-l', '--lock-wait',
        metavar='lock_wait',
        required=False,
        help='wait at most SECONDS for acquiring a repository/cache. Default 1.',
        default=1
    )
    parser.add_argument(
        '-t', '--textfile',
        metavar='textfile',
        required=False,
        help='File for write collected data',
        default='test.prom'
    )
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    registry = CollectorRegistry()
    
    list_command = subprocess.getstatusoutput(
        "borg list --json --lock-wait {} {}".format(args.lock_wait, args.repository))
    if list_command[0]:
        sys.exit(list_command[0])
    archives = json.loads(list_command[1])

    info_command = subprocess.getstatusoutput(
        "borg info --last 1 --json --lock-wait {} {}".format(args.lock_wait, args.repository))
    if info_command[0]:
        sys.exit(info_command[0])
    info = json.loads(info_command[1])

    Gauge('backup_count', 'backups count', ['host', 'location', 'repo'],
          registry=registry).labels(info['archives'][0]['hostname'],
                                    args.repository, args.repo_name).set(len(archives['archives']))
    Gauge('backup_files', 'backups count', ['host', 'location', 'repo'],
          registry=registry).labels(info['archives'][0]['hostname'],
                                    args.repository, args.repo_name).set(info['archives'][0]['stats']['nfiles'])

    backup_last = {'backup_last_size': 'original_size',
                   'backup_last_size_compressed': 'compressed_size',
                   'backup_last_size_dedup': 'deduplicated_size'}
    for bl in backup_last:
        Gauge(bl, '', ['host', 'location', 'repo'],
              registry=registry).labels(info['archives'][0]['hostname'],
                                        args.repository, args.repo_name).set(info['archives'][0]['stats'][backup_last[bl]])

    cache_stat = {'backup_chunks_unique': 'total_unique_chunks',
                  'backup_chunks_total': 'total_chunks', 'backup_total_size': 'total_size',
                  'backup_total_size_compressed': 'total_csize',
                  'backup_total_size_dedup': 'unique_csize'}
    for cs in cache_stat:
        Gauge(cs, '', ['host', 'location', 'repo'],
              registry=registry).labels(info['archives'][0]['hostname'],
                                        args.repository, args.repo_name).set(info['cache']['stats'][cache_stat[cs]])

    write_to_textfile(args.textfile, registry)