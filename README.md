# Borg exporter

Export borg information to prometheus.

## Dependencies

* Prometheus (obviously)
* Node Exporter with textfile collector
* [Borg](https://github.com/borgbackup/borg)
* Prometheus python client  - prometheus_client

## Role Variables

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: prometheus_borg_exporter
      prometheus_borg_repo_name: borg-edge
      prometheus_borg_repo: /u01/borgbackup/borg-edge
```

## Configure your node exporter

Make sure your node exporter uses `textfile` in `--collectors.enabled` and add the following parameter: `--collector.textfile.directory=/var/lib/node_exporter/textfile_collector`

## Example queries

```ini
backup_last_size_compressed{host="sbt-opir-0348",location="ssh://borg@10.116.99.92/u01/borg-erib",repo="borg-erib"} 485671201.0
```

### Grafana dashboard

See [here](https://grafana.net/dashboards/1573) for a sample grafana dashboard.
BSD
